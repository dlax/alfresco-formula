{% from "alfresco/map.jinja" import alfresco with context %}

tomcat7:
  pkg:
    - installed

libpostgresql-jdbc-java:
  pkg:
    - installed

/usr/share/tomcat7/lib/postgresql.jar:
  file.symlink:
    - target: /usr/share/java/postgresql.jar
    - require:
      - pkg: libpostgresql-jdbc-java
      - pkg: tomcat7

patch:
  pkg:
    - installed

/etc/tomcat7/server.xml:
  file.patch:
    - source: salt://alfresco/tomcat7-set-maxHttpHeaderSize.patch
    - hash: md5=309a5de344345353b3ca0a30bca58a90
    - require:
      - pkg: tomcat7
      - pkg: patch

/etc/tomcat7/context.xml:
  file.patch:
    - source: salt://alfresco/tomcat7-context.patch
    - hash: md5=0c960a2e5f1b9d9163730693d6c2dbb0
    - require:
      - pkg: tomcat7
      - pkg: patch

tomcat7_setenv:
  file.managed:
    - name: /usr/share/tomcat7/bin/setenv.sh
    - source: salt://alfresco/tomcat7-setenv.sh
    - require:
      - pkg: tomcat7

unzip:
  pkg:
    - installed

opt_dir:
  file.directory:
    - name: /opt
    - user: root
    - group: root
    - recurse:
      - user
      - group

alfresco-dist:
  archive:
    - name: {{ alfresco.dist_root }}
    - extracted
    - source: {{ alfresco.dist_url }}
    - hash: {{ alfresco.dist_hash }}
    - archive_format: zip
    - if_missing: {{ alfresco.dist_root }}
    - require:
      - pkg: unzip
      - file: opt_dir

/var/lib/tomcat7/webapps:
  file.directory:
    - user: tomcat7
    - group: tomcat7
    - recurse:
      - user
      - group
    - require:
      - pkg: tomcat7

{% for warfile in ('share.war', 'alfresco.war') %}
/var/lib/tomcat7/webapps/{{ alfresco.tomcat_context }}{{ warfile }}:
  file.copy:
    - source: {{ alfresco.dist_root }}/web-server/webapps/{{ warfile }}
    - require:
      - archive: alfresco-dist
      - pkg: tomcat7
      - file: /var/lib/tomcat7/webapps
{% endfor %}

{% for dir in ('vti_bin', 'alfresco', 'ROOT', 'share', 'solr4') %}

/var/lib/tomcat7/webapps/{{ dir }}:
  file:
    - absent

{% endfor %}

{% set alf_dirroot = '/opt/alfresco_root/alf_data' %}
{{ alf_dirroot }}:
  file.directory:
    - makedirs: True
    - user: tomcat7
    - group: tomcat7
    - require:
      - pkg: tomcat7

/var/lib/tomcat7/shared/classes/alfresco-global.properties:
  file.managed:
    - source: salt://alfresco/alfresco-global.properties
    - template: jinja
    - context:
      dirroot: {{ alf_dirroot }}

include:
  - .supervisor
