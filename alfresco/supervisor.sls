{% from "alfresco/map.jinja" import alfresco with context %}

supervisor:
  pkg:
    - installed

/etc/supervisor/conf.d/tomcat7.conf:
  file.managed:
    - source: salt://alfresco/tomcat7-supervisord.conf
    - template: jinja
