{% from "alfresco/map.jinja" import alfresco with context %}

alfresco_installer_deps:
  pkg.installed:
    - pkgs:
      - fontconfig
      - libsm6
      - libice6
      - libxrender1
      - libxext6

/root/alfresco_installer.bin:
  file.managed:
    - source: {{ alfresco.installer }}
    - hash: {{ alfresco.installer_hash }}
    - mode: 0744

/root/alfresco_installer_options:
  file.managed:
    - source: salt://alfresco/alfresco_installer_options
    - template: jinja
    - mode: 0600

alfresco_install:
  cmd.run:
    - name: ./alfresco_installer.bin --optionfile /root/alfresco_installer_options
    - unless: test -d {{ alfresco.installdir }}
    - require:
      - file: /root/alfresco_installer_options
      - file: /root/alfresco_installer.bin
      - pkg: alfresco_installer_deps

{{ alfresco.installdir }}/tomcat/bin/setenv.sh:
  file.append:
    - text: CATALINA_OPTS=-Djava.security.egd=file:/dev/./urandom
    - require:
      - cmd: alfresco_install

{{ alfresco.tomcat_user }}:
  user.present:
    - home: {{ alfresco.installdir }}/tomcat
    - shell: /bin/false
    - system: True

{% for tomcat_dir in ('logs', 'work', 'webapps') %}
{{ alfresco.installdir }}/tomcat/{{ tomcat_dir }}:
  file.directory:
    - user: {{ alfresco.tomcat_user }}
    - recurse:
      - user
{% endfor %}

{{ alfresco.installdir }}/alf_data:
  file.directory:
    - user: {{ alfresco.tomcat_user }}
    - recurse:
      - user

{% for wardir in ('share', 'alfresco') %}
{{ alfresco.installdir}}/tomcat/webapps/{{ wardir }}:  # XXX Insert tomcat_context here
  file:
    - absent
{% endfor %}

include:
  - .supervisor
