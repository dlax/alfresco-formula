#!/bin/sh

. /etc/default/tomcat7

NAME=tomcat7

CATALINA_BASE=/var/lib/tomcat7
CATALINA_HOME=/usr/share/tomcat7
JVM_TMP=/tmp/tomcat7-$NAME-tmp
CATALINA_TMPDIR="$JVM_TMP"
if [ ! -d $CATALINA_TMPDIR ]; then
    mkdir $CATALINA_TMPDIR
    chown -R tomcat7: $CATALINA_TMPDIR
fi
CATALINA_OUT=/var/log/tomcat7/catalina.out
CATALINA_OPTS=-Djava.security.egd=file:/dev/./urandom
JAVA_HOME=/usr/lib/jvm/default-java
export JAVA_HOME  # used by JRE_HOME
#JAVA_OPTS=
#CLASSPATH:       /usr/local/tomcat/bin/bootstrap.jar:/usr/local/tomcat/bin/tomcat-juli.jar

